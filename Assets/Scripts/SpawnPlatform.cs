﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPlatform : MonoBehaviour {
    
    [SerializeField] List<GameObject> platform;
    private int platformCount;

    private void Start()
    {
        platformCount = platform.Count;
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        Instantiate(platform[Random.Range(0, platformCount)], transform.position, transform.rotation, transform);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeroController : MonoBehaviour {

    private enum AuraEffect {Fire = 0, Ice = 1, Nan=2}

    [SerializeField] private GameObject fireAura;
    [SerializeField] private GameObject iceAura;
    [SerializeField] private GameController gameController;
    [SerializeField] private Animator animator;
    [SerializeField] private AuraEffect auraHero;

    private Rigidbody2D ninjaHero;
    private bool _isGround;
    private bool dualTab;

    void Start () {
        ninjaHero = this.gameObject.GetComponent<Rigidbody2D>();
        gameController = GetComponentInParent<GameController>();

        auraHero = AuraEffect.Fire;
        fireAura.SetActive(true);
        dualTab = false; //отслеживать двойной таб в воздухе
        _isGround = true;
    }

	void Update () {
        if (gameController.CheckGameOver()) return;

#if UNITY_IOS || UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                InputHero();
            }
        }
#else
        if (Input.GetKeyUp(KeyCode.Space))
        {
            InputHero();
        }
#endif
    }

    private void InputHero()
    {
        if (_isGround)
        {
            ninjaHero.velocity = Vector2.zero;
            ninjaHero.AddForce(new Vector2(0, 400));

            _isGround = false;
            dualTab = false;
        }
        else
        {
            if (dualTab)
            {
                SwitchAura();
                dualTab = false;
            }
            else
            {
                dualTab = true;
            }
        }
    }

    /// <summary>
    /// Переключение ауры персонажа
    /// </summary>
    private void SwitchAura()
    {
        if(auraHero == AuraEffect.Fire)
        {
            auraHero = AuraEffect.Ice;
            iceAura.SetActive(true);
            fireAura.SetActive(false);
        }
        else
        {
            auraHero = AuraEffect.Fire;
            iceAura.SetActive(false);
            fireAura.SetActive(true);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (gameController.CheckGameOver()) return;

        if (auraHero == AuraPlatform(collision.gameObject.tag))
        {
            _isGround = true;
            AnimationJump(false);
        }
        else
        {
            gameController.SetGameOver(true);
            animator.SetTrigger("Died");
            iceAura.SetActive(false);
            fireAura.SetActive(false);
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (gameController.CheckGameOver()) return;

        _isGround = false;
        AnimationJump(true);
    }

    private void AnimationJump(bool flag)
    {
        animator.SetBool("Jump", flag);
    }

    /// <summary>
    /// Узнаем ауру текущей платформы
    /// </summary>
    /// <param name="value">Тэе платформы</param>
    /// <returns></returns>
    private AuraEffect AuraPlatform(string value)
    {
        if (value == "Fire")
        {
            return AuraEffect.Fire;
        }else if(value == "Ice")
        {
            return AuraEffect.Ice;
        }
        else
        {
            return AuraEffect.Nan;
        }
    }
}

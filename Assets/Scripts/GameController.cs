﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour {

    [SerializeField] GameObject hero;
    [SerializeField] private Text textPoints; // счетчик пройденных платформ в углу экрана
    [SerializeField] private Text resultTextPoints; // количество пройденных платформ в конце игры
    [SerializeField] private GameObject panelMenu;
    [SerializeField] [Range(-6, -3)] private float speed = -3.5f;

    private int points = 0;
    private bool gameOver;

    public float Speed
    {
        get
        {
            return this.speed;
        }
        private set
        {
            if (value > -3)
            {
                this.speed = -3;
            }
            else if (value < -6)
            {
                this.speed = -6;
            }
            else
            {
                this.speed = value;
            }
            
        }
    }

    void Start () {
        Speed = speed;
        panelMenu.SetActive(false);
        gameOver = false;
    }

    public float GetHeroPositionX()
    {
        return hero.transform.position.x;
    }

    public void SetPoints()
    {
        points++;
        textPoints.text = points.ToString();
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(0);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void SetGameOver(bool value)
    {
        gameOver = value;
         if (value == true)
        {
            panelMenu.SetActive(true);
            resultTextPoints.text = "Ваш результат: " + points.ToString();
        }
        else
        {
            panelMenu.SetActive(false);
        }
    }

    public bool CheckGameOver()
    {
        return gameOver;
    }
}

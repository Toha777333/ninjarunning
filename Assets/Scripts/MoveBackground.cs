﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBackground : MonoBehaviour
{
    private const float valueOffsetDenomintion = 1f; //Смещение фона происходит в пределах от 0 до 1

    [SerializeField] private MeshRenderer firstBG;
    [SerializeField] private float firstBGSpeed = 0.01f;

    [SerializeField] private MeshRenderer secondBG;
    [SerializeField] private float secondBGSpeed = 0.05f;

    [SerializeField] private MeshRenderer thirdBG;
    [SerializeField] private float thirdBGSpeed = 0.1f;

    Vector2 offset;

    void Move(MeshRenderer mesh, float speed)
    {
        offset = Vector2.zero;
        float tmp = Mathf.Repeat(Time.time * speed, 1);
        offset = new Vector2(tmp, 0);
        mesh.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }

    void Update()
    {
        if (firstBG) Move(firstBG, firstBGSpeed);
        if (secondBG) Move(secondBG, secondBGSpeed);
        if (thirdBG) Move(thirdBG, thirdBGSpeed);
    }

    /// <summary>
    /// Устанавливаем скорость фона третьего уровня (ближнего)
    /// </summary>
    /// <param name="_speed"></param>
    public void SetThirdSpeed(float _speed)
    {
        thirdBGSpeed = valueOffsetDenomintion /_speed;
    }

    private void OnDestroy()
    {
        offset = Vector2.zero;
        if (firstBG) firstBG.sharedMaterial.SetTextureOffset("_MainTex", offset);
        if (secondBG) secondBG.sharedMaterial.SetTextureOffset("_MainTex", offset);
        if (thirdBG) thirdBG.sharedMaterial.SetTextureOffset("_MainTex", offset);
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour {
    private GameController gameController;
    private float speed = -3.5f;
    private float heroPosition;
    private bool flagPoints = true;

    void Start () {
        gameController = GetComponentInParent<GameController>();
        heroPosition = gameController.GetHeroPositionX();
    }

	void Update () {
        if (gameController.CheckGameOver()) return;

        speed = gameController.Speed;
        transform.Translate(speed * Time.deltaTime, 0, 0);

        if (flagPoints == true && heroPosition >= this.transform.position.x)
        {
            gameController.SetPoints();
            flagPoints = false;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Lava")
        {
            Destroy(this.gameObject);
        }
    }
}
